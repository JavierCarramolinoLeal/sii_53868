// Autor: Javier Carramolino Leal 53868
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <pthread.h>
#include "Esfera.h"
#include "Socket.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	//Esfera esfera;
	//Creamos el vector de esfera
	std::vector<Esfera> esfera;
	std::vector<Plano> paredes;
	std::vector<Esfera> disparos1;
	std::vector<Esfera> disparos2;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	//AÑADIMOS DATOSMEMCOMP
	//DatosMemCompartida datosm;
	//DatosMemCompartida *datosmpunt; //puntero a datosmemcompartida
	char *org;
	int rebotes;

	int puntos1;
	int puntos2;

	int tuberia;

	pthread_t thid1;

	Socket s_conexion;
	Socket s_comunicacion;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
